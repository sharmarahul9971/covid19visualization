
export const indiaTableFilter:any = {
  "label":["state","confirmed","active","recovered","deaths"],
  // "dataField":["district","data.confirmed","data.active","data.recovered","data.deceased"],
  "caption":["State/UT","Confirmed","Active","Recovered","Deceased"],
  "todayReport":["","deltaconfirmed","","deltarecovered","deltadeaths"],
  "headerColor":["violet","rgb(255, 7, 58)","orange","brown","#99A1A8"],
  "dataColor":["brown","black","black","black","black"],
  "visible":[true,true,true,true,true],
  "filter":[true,false,false,false,false],
  "headerFiltering":[true,false,false,false,false],
  "width":[],
  "sort":["","desc","","",""],
  "desc":["","desc","","",""],
  "todayCasesInfo":["","Today Confirmed Cases till Last Update","","Today Recovered till Last Update","Today Deaths till Last Update",],
  "align":["left","center","center","center","center"],
  "pagerInfo":"Page #{0}. Total: {1} ( {2} States / UT )",
  "masterDetail":"indiaData",
  "masterDetailEnable":true
}


export const countriesTableFilter:any = {
  "label":['flag',"country","cases","active","recovered","deaths","critical","casesPerOneMillion","deathsPerOneMillion"],
  "dataField":["flag","country","cases","active","recovered","deaths","critical","casesPerOneMillion","deathsPerOneMillion"],
  "caption":["","Country","Confirmed","Active","Recovered","Deceased","Critical","Cases Per One Million","Deceased Per One Million"],
  "todayReport":["","","todayCases","","","todayDeaths","","",""],
  "headerColor":["","","#ff073a","blue","#28a745 ","#6c757d" ,"pink","purple","brown"],
  "dataColor":["","brown","black","black","black","black","red","",""],
  "visible":[true,true,true,true,true,true,true,false,false],
  "filter":["",true,false,false,false,false,false,false,false],
  "headerFiltering":["",true,false,false,false,false,false,false,false],
  "width":[40],
  "sort":["","","desc","","","","","",""],
  "todayCasesInfo":["","","Today Cases till Last Update","","","Today Deaths till Last Update","","",""],
  "align":["","left","center","center","center","center","center","center","center"],
  "pagerInfo":"Page #{0}. Total: {1} ( {2} Countries )",
  "masterDetail":"countriesData",
  "masterDetailEnable":true
}

export const indiaDistrictTableFilter:any = {
  "label":["district","confirmed","active","recovered","deceased"],
  "dataField":["district","data.confirmed","data.active","data.recovered","data.deceased"],
  "caption":["District","Confirmed","Active","Recovered","Deceased"],
  "todayReport":["","deltaconfirmed","","deltarecovered","deltadeaths"],
  "headerColor":["violet","rgb(255, 7, 58)","orange","brown","#99A1A8"],
  "dataColor":["brown","black","black","black","black"],
  "visible":[true,true,true,true,true],
  "filter":[true,false,false,false,false],
  "headerFiltering":[true,false,false,false,false],
  "width":[],
  "sort":["","desc","","",""],
  "desc":["","desc","","",""],
  "todayCasesInfo":["","Today Confirmed Cases till Last Update","","Today Recovered till Last Update","Today Deaths till Last Update",],
  "align":["left","center","center","center","center"],
  "pagerInfo":"Page #{0}. Total: {1} ( {2} District / Unknown / Other State )",
  "masterDetail":"indiaDistrictData",
  "masterDetailEnable":false
}
export const indiaLineFilter:any = {
  "xAxisLabel":["date"],
  "label": ['Confirmed','Recovered','Deceased'],
  "fill": [false,false,false],
  "pointRadius"	:[2,2,2],
  "pointStyle":['dash','dash','dash'],
  "fillcolor": ["red","brown",'blue'],
  "borderColor": ["red","brown",'blue'],
  "borderWidth": [3,3,3],
  "data": ["totalconfirmed","totalrecovered","totaldeceased"],
  "pointBackgroundColor":["red","brown",'blue'],
  "pointBorderColor":['rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)'],
  "title":"Covid - 19 Cases ( India ) "
}
export const indiaDailyLineFilter:any = {
  "xAxisLabel":["date"],
  "label": ['Confirmed','Recovered','Deceased'],
  "fill": [false,false,false],
  // "pointRadius"	:[2,2,2],
  // "pointStyle":['dash','dash','dash'],
  "fillcolor": ["red","brown",'blue'],
  "borderColor": ["red","brown",'blue'],
  "borderWidth": [3,3,3],
  "data": ["dailyconfirmed","dailyrecovered","dailydeceased"],
  "backgroundColor":["red","brown",'blue'],
  // "pointBorderColor":['rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)'],
  "title":"Covid - 19 Daily Cases ( India ) "
}

// export const
export const masterDetail = ["indiaData","countriesData"]

export const getKeyByValue = (object:any, value:any) => {
  return [object[value]]
}
