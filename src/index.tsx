import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import { blue } from '@material-ui/core/colors';
import App from './App'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from './appStore/store'

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: {
      main:'#ffffff'
    }
  }
});

ReactDOM.render(
  <BrowserRouter >
    <ThemeProvider theme={theme}>
      <Switch>
        <Provider store={store}><Route path="/" component={App}/></Provider>
      </Switch>
    </ThemeProvider>
  </BrowserRouter>
  ,document.getElementById('root'));

serviceWorker.unregister();
