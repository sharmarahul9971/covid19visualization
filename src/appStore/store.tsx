import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { initialState } from "./initialState";

function reducer(state = initialState, action: any) {
  // console.log(state)
  switch(action.type) {
    case 'SETCOUNTRYDATA':
      return {
        data: {
          "global":state.data.global,
          "country":action.data,
          "india":state.data.india,
          "graphglobal":state.data.graphglobal,
          "statedata":state.data.statedata,
          "dailystatedata":state.data.dailystatedata
        }
      };
    case 'SETGLOBALDATA':
      return {
        data: {
          "india":state.data.india,
          "global":action.data,
          "country":state.data.country,
          "graphglobal":state.data.graphglobal,
          "statedata":state.data.statedata,
          "dailystatedata":state.data.dailystatedata
        }
      }
    case 'SETINDIADATA':
      return {
        data: {
          "country":state.data.country,
          "global":state.data.global,
          "india":action.data,
          "graphglobal":state.data.graphglobal,
          "statedata":state.data.statedata,
          "dailystatedata":state.data.dailystatedata
        }
      }
    case 'SETGRAPHGLOBALDATA':
      return {
        data: {
          "global":state.data.global,
          "country":state.data.country,
          "india":state.data.india,
          "graphglobal":action.data,
          "statedata":state.data.statedata,
          "dailystatedata":state.data.dailystatedata
        }
      };
    case 'SETSTATEDATA':
      return {
        data: {
          "global":state.data.global,
          "country":state.data.country,
          "india":state.data.india,
          "graphglobal":state.data.graphglobal,
          "statedata":action.data,
          "dailystatedata":state.data.dailystatedata
        }
      };
      case 'SETDAILYSTATEDATA':
        return {
          data: {
            "india":state.data.india,
            "global":state.data.global,
            "country":state.data.country,
            "graphglobal":state.data.graphglobal,
            "statedata":state.data.statedata,
            "dailystatedata":action.data
          }
        }
    default:
        return state;
  }
}

const composeEnhancers =
  typeof window === 'object' &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
}) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(thunk),
);

export const store = createStore(reducer, enhancer)
