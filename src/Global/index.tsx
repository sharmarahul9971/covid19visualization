import React from "react"
import Table from "./../Component/Table";
import { connect } from 'react-redux';
import { setCountryData } from "./../appStore/actions";
import { countriesTableFilter }from "./../DeclarationFile";
import { Grid,Paper ,Typography} from '@material-ui/core';

class Global extends React.Component<any,any>{
  constructor(props:any){
    super(props);
  }
  componentDidMount(){
    fetch("https://corona.lmao.ninja/v2/countries")
    .then((res:any) => {
      return res.json()
    }).then((data) => {
      let dataCountry:any = [];
      let waste = data.map((key:any,value:any)=>{
        dataCountry.push({
          "flag":key.countryInfo.flag,
          "country":key.country,
          "cases":key.cases,
          "active":key.active,
          "recovered":key.recovered,
          "deaths":key.deaths,
          "critical":key.critical,
          "casesPerOneMillion":key.casesPerOneMillion,
          "deathsPerOneMillion":key.deathsPerOneMillion,
          "iso2":key.countryInfo.iso2,
          "todayCases":key.todayCases,
          "todayDeaths":key.todayDeaths,
          "masterDetail":"countriesData",
        })
      })
      this.props.setCountryData(dataCountry);
    })
  }

  render(){
    return(
      <Grid
        container
        spacing={2}
        >
        <Grid item xs={12}>
          <Paper elevation={3} >
            <Grid style={{padding:"10px",background:"#2196f3",borderRadius:"5px",color:"white"}}>
              <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h5"}>World Report</Typography>
            </Grid>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Table data={this.props.data.country} template={countriesTableFilter}/>
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = (state: any) => {
  return {
    data: state.data
  };
}

const mapDispatchToProps = {
  setCountryData
};

export default connect(mapStateToProps,mapDispatchToProps)(Global);
