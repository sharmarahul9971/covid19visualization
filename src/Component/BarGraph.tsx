import React from 'react';
import { Line ,Bar } from 'react-chartjs-2';
import { connect } from "react-redux";

class BarGraph extends React.Component<any,any>{
  render(){
    let dataJson:any = []
    let data = this.props.filter.label.map((key:any,value:any)=>{
      dataJson.push({
        "label": key,
        "fill": this.props.filter.fill[value],
        "fillcolor": this.props.filter.fillcolor[value],
        "borderColor": this.props.filter.borderColor[value],
        "borderWidth": this.props.filter.borderWidth[value],
        "data": this.props.data[this.props.filter.data[value]],
        "backgroundColor":this.props.filter.backgroundColor[value],
      })
    })
    const state = {
      labels: this.props.data[this.props.filter.xAxisLabel],
      datasets: dataJson
    }
    return(
      <div>
        <Bar
          data={state}
          options={{
            title:{
              display:true,
              text:this.props.filter.title                                                                                          ,
              fontSize:20
            },
            tooltips: {
                  callbacks: {
                    label: function(tooltipItem:any, data:any) {
                        return " "+data.datasets[tooltipItem.datasetIndex].label+" : "+parseInt(tooltipItem.value).toLocaleString();
                    }
                  },
                  mode: 'index',
                  intersect:false
                },
            legend:{
              display:true,
              position:'right'
            }
          }}
        />
      </div>
    )
  }
}

export default BarGraph;
