import React ,{Component} from 'react';
import { connect } from 'react-redux';
import { Box, Grid } from '@material-ui/core';
import { Line } from 'react-chartjs-2';
import moment from 'moment';

class LineGraph extends React.Component<any,any>{
  render(){
      // console.log(this.props.data.graphglobal)
      var data:any = [];
      let d  = this.props.data.graphglobal!==null?Object.keys(this.props.data.graphglobal.cases).map((key:any)=>{
          data.push(moment(key).format("DD MMM YYYY"))
      }):[]
      const state = {
        labels:data,
        datasets: [
          {
            label: 'Confirmed',
            fill: false,
            pointRadius	:2,
            pointStyle:'dash',
            fillColor: "rgba(151,187,205,0.2)",
            pointBackgroundColor:"red",
            borderColor: 'red',
            borderWidth: 3,
            pointHoverBackgroundColor	:"red",
            pointHoverBorderColor	:"red",
            pointBorderColor:'rgba(0,0,0,0)',
            data: this.props.data.graphglobal!==null?Object.values(this.props.data.graphglobal.cases):[]
          },
          {
            label: 'Recovered',
            fill: false,
            pointRadius	:2,
            pointStyle:'dash',
            fillcolor: 'rgb(40, 167, 69)',
            borderColor: 'rgb(40, 167, 69)',
            borderWidth: 3,
            data: this.props.data.graphglobal!==null?Object.values(this.props.data.graphglobal.recovered):[],
            pointBackgroundColor:'rgb(40, 167, 69)',
            pointBorderColor:'rgba(0,0,0,0)',
          },
          {
            label: 'Deceased',
            fill: false,
            pointRadius	:2,
            pointStyle:'dash',
            fillcolor: 'blue',
            borderColor: 'blue',
            borderWidth: 3,
            data:this.props.data.graphglobal!==null? Object.values(this.props.data.graphglobal.deaths):[],
            pointBackgroundColor:'blue',
            pointBorderColor:'rgba(0,0,0,0)',
          }
        ]
      }
      return(
        <Grid
          container
          direction={"row"}
          alignItems={"center"}
          >
          <Grid item xs={6}></Grid>
          <Grid item xs={12}>
            <Box>
                <Line
                  data={state}
                  options={{
                    responsive:true,
                    title:{
                      display:true,
                      text:'Covid - 19 World Growth in Last 30 Days',
                      fontSize:20
                    },
                    legend:{
                      display:true,
                      position:'right'
                    },
                    tooltips: {
                        callbacks: {
                          label: function(tooltipItem:any, data:any) {
                              return " "+data.datasets[tooltipItem.datasetIndex].label+" : "+data.datasets[0].data[tooltipItem.index].toLocaleString();
                          }
                        },
                      mode: 'nearest',
                      intersect:false
                    },
                    hover: {
                       mode: 'index',
                       intersect: true
                    }
                  }}
                />
              </Box>
            </Grid>
          <Grid item xs={2}></Grid>
        </Grid>
      )
    }
}

const mapStateToProps = (state: any) => {
  return {
    data: state.data
  };
}

export default connect(mapStateToProps)(LineGraph)
