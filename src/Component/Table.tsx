import React from 'react';
import { connect } from 'react-redux';
import { Grid,Paper } from '@material-ui/core';
import DataGrid, {
  Column,
  Grouping,
  GroupPanel,
  Pager,
  Paging,
  MasterDetail,
  SearchPanel,
  HeaderFilter,
  FilterRow,
  Summary,
  TotalItem,
  Export,
  ColumnChooser,
  StateStoring
} from 'devextreme-react/data-grid';
import "./Styles.css";
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import LineGraph from './Line';
import Master from "./Master"

class Table extends React.Component<any,any>{
  render(){
    let dataRow = this.props.template.label.map((key:any,value:any)=>{
      return(
        <Column
          key={value}
          dataField={key}
          alignment={this.props.template.align[value]}
          caption={this.props.template.caption[value]}
          allowReordering={false}
          defaultSortOrder={this.props.template.sort[value]}
          allowFiltering={this.props.template.filter[value]}
          allowEditing={false}
          allowSorting={true}
          width={this.props.template.width[value]}
          visible={this.props.template.visible[value]}
          allowHeaderFiltering={this.props.template.headerFiltering[value]}
          cellRender={(cell:any)=>{
            return(
                  key==="flag"?<img width={20} alt={''} height={20} src={cell.data[key]}></img>:
                  (
                  <div
                    className={cell.data[this.props.template.todayReport[value]] > 0 ? 'inc' : 'dec'}
                    >
                    <div
                      style={{marginLeft:"2px",fontWeight:"bold",float:"left",color:this.props.template.dataColor[value]}}
                      className="current-value"
                      >
                      {(cell.data[key]!==undefined)?cell.data[key].toLocaleString():""}
                    </div>
                    <div
                      title={this.props.template.todayCasesInfo[value]}
                      style={{paddingLeft:"5px"}}
                      className="diff"
                      >
                      {
                        ((cell.data[this.props.template.todayReport[value]]!==0 && cell.data[this.props.template.todayReport[value]]!==undefined)
                        ?cell.data[this.props.template.todayReport[value]].toLocaleString()
                        :""
                      )
                    }
                    </div>
                  </div>
                )
            )
          }}
          headerCellRender={(data:any)=>{
              return(<p
                style={{fontWeight:"bold",color:this.props.template.headerColor[value]}}
                >
                {data.column.caption}
              </p>)
            }
          }
        />
      )
    })

    return(
      <Paper style={{padding:"10px"}} elevation={3} >
        <DataGrid
          id="gridContainer"
          dataSource={this.props.data}
          showBorders={true}
          hoverStateEnabled={true}
          selection={{ mode: isWidthUp("sm",this.props.width)?'multiple':'single' }}
          remoteOperations={true}
          >
          <ColumnChooser enabled={true} />
          <GroupPanel visible={isWidthUp("sm",this.props.width)?true:false} />
          <SearchPanel visible={true} />
          <HeaderFilter visible={true} />
          <FilterRow visible={true}/>
          <Grouping autoExpandAll={false} />
          {dataRow}
          <Paging defaultPageSize={10} />
          <MasterDetail
            enabled={this.props.template.masterDetailEnable}
            render={(cell:any)=>{
              return(<Master masterDetail={cell.data}/>)
            }}
          />
          <Pager
            allowedPageSizes={[10,20]}
            showPageSizeSelector={true}
            showInfo={true}
            showNavigationButtons={true}
            infoText={this.props.template.pagerInfo}
          />
          <Export enabled={true} allowExportSelectedData={true}/>
        </DataGrid>
      </Paper>
    )
  }
}
export default withWidth()(Table);
