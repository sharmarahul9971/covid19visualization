import React from 'react';
import {Pie, Doughnut} from 'react-chartjs-2';
import { connect } from 'react-redux';
import { Grid , Typography} from '@material-ui/core';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import MainPage from './mainPage'

class PieGraph extends React.Component<any,any> {
  render() {
    // console.log(this.props.data.global)
    var data:any = (this.props.data.global!==null && this.props.data!==undefined )?[
        this.props.data.global.active,
        this.props.data.global.recovered,
        this.props.data.global.deaths
      ]:[]

    const state = {
      labels: ['Active', 'Recovered',
               'Deaths'],
      datasets: [
        {
          label: 'Covid-19 Global Cases',
          backgroundColor: ['#2FDE00','#00A6B4','#6800B4'],
          hoverBackgroundColor: ['#175000','#003350','#35014F'],
          data:data
        }
      ]
    }

    return (
      <div style={{margin:"10px"}}>
        <Grid container >
          <Grid item xs={12}>
            <Pie
              data={state}
              options={{
                responsive	:true,
                title:{
                  display:true,
                  text:'Covid-19 Global Cases',
                  fontSize:20
                },
                legend:{
                  display:true,
                  position:'bottom'
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem:any, data:any) {
                            return " "+data.labels[tooltipItem.index]+" : "+data.datasets[0].data[tooltipItem.index].toLocaleString();
                        }
                    }
                }
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <MainPage/>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    data: state.data
  };
}

export default withWidth()(connect(mapStateToProps)(PieGraph));
