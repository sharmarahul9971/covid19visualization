import React from "react"
import DistrictUT from './../India/DistrictUT'
import LineGraph from './Line';
import { masterDetail } from "./../DeclarationFile"

class Master extends React.Component<any,any>{
  render(){
      let display = (this.props.masterDetail["masterDetail"]===masterDetail[0])?
      <DistrictUT stateDetail={this.props.masterDetail}/>:<LineGraph countryDetail={this.props.masterDetail}/>
    return(
      <div>
        {
          display
        }
      </div>
    )
  }
}

export default Master
