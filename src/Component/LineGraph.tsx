import React from "react";
import { connect } from 'react-redux';
import { Line } from 'react-chartjs-2';

class LineGraph extends React.Component<any,any>{
  constructor(props:any){
    super(props);
  }
  render(){
    // console.log(this.props)
    let dataJson :any = []
    let data = this.props.filter.label.map((key:any,value:any)=>{
      dataJson.push({
      "label": key,
      "fill": this.props.filter.fill[value],
      "pointRadius"	:this.props.filter.pointRadius[value],
      "pointStyle":this.props.filter.pointStyle[value],
      "fillcolor": this.props.filter.fillcolor[value],
      "borderColor": this.props.filter.borderColor[value],
      "borderWidth": this.props.filter.borderWidth[value],
      "data": this.props.data[this.props.filter.data[value]],
      "pointBackgroundColor":this.props.filter.pointBackgroundColor[value],
      "pointBorderColor":this.props.filter.pointBorderColor[value]
    })
  })
    // console.log(dataJson)
    const state = {
      labels: this.props.data[this.props.filter.xAxisLabel],
      datasets:dataJson
    }
    return(
      <div>
        <Line
        data={state}
        options={{
          responsive:true,
          title:{
            display:true,
            text:this.props.filter.title,
            fontSize:20
          },
          legend:{
            display:true,
            position:'right'
          },
          tooltips: {
            callbacks: {
              label: function(tooltipItem:any, data:any) {
                // console.log(tooltipItem)
                  return " "+data.datasets[tooltipItem.datasetIndex].label+" : "+parseInt(tooltipItem.value).toLocaleString();
              }
            },
            mode: 'index',//nearest index
            intersect:false
          }
        }}
      />
      </div>
    )
}
}

export default LineGraph;
