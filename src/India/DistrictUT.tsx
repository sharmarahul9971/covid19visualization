import React from 'react';
import { connect } from 'react-redux';
import { TabPanel, Item } from 'devextreme-react/tab-panel';
import DataGridTable from './DataGrid'
import LineGraph from "./LineGraph";

class DistrictUT extends React.Component<any,any>{
    render(){
      // console.log(th)
      return(
        <TabPanel
          itemTitleRender={(key:any)=><span style={{color:"brown",fontWeight:"bold"}}>{key.title}</span>}
          animationEnabled={true}
          >
          <Item title="District Wise Data" render={()=><DataGridTable stateDetail={this.props.stateDetail}/>} />
          <Item title="Visualize State Data" render={()=><LineGraph stateDetail={this.props.stateDetail}/>} />
        </TabPanel>
      )
    }
}

export default DistrictUT;
