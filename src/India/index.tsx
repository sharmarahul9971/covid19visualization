import React from 'react';
import { Grid,Paper } from '@material-ui/core';
import { setIndiaData ,setStateData , setDailyStateData } from "./../appStore/actions";
import { connect } from 'react-redux';
import { indiaLineFilter , indiaTableFilter , indiaDailyLineFilter } from "./../DeclarationFile";
import Table from "./../Component/Table";
import LineGraph from "./../Component/LineGraph";
import BarGraph from "./../Component/BarGraph";
import TopTab from './TopTab'
import { Route,  Link } from 'react-router-dom';


class India extends React.Component<any,any>{
  componentDidMount(){
    fetch("https://api.covid19india.org/data.json")
    .then((res:any)=>res.json())
    .then((data:any)=>{
      this.props.setIndiaData(data)
    })
    fetch("https://api.covid19india.org/state_district_wise.json")
    .then((res:any)=>res.json())
    .then((data:any)=>{
      this.props.setStateData(data)
    })
    fetch("https://api.covid19india.org/states_daily.json")
    .then((res:any)=>res.json())
    .then((data:any)=>{
      this.props.setDailyStateData(data)
    })
  }

  render(){
    console.log(this.props)
    var data:any = []
    let dataJson:any = []
    let indiaData:any = []

    dataJson["date"] = []
    dataJson["totalconfirmed"] = []
    dataJson["totaldeceased"] = []
    dataJson["totalrecovered"] = []
    dataJson["dailyconfirmed"] = []
    dataJson["dailydeceased"] = []
    dataJson["dailyrecovered"] = []


    let _dataJson = this.props.data.india!==null?this.props.data.india.cases_time_series.map((key:any,value:any)=>{
      dataJson["date"].push(key.date)
      dataJson["totalconfirmed"].push(key.totalconfirmed)
      dataJson["totaldeceased"].push(key.totaldeceased)
      dataJson["totalrecovered"].push(key.totalrecovered)
      dataJson["dailyconfirmed"].push(key.dailyconfirmed)
      dataJson["dailydeceased"].push(key.dailydeceased)
      dataJson["dailyrecovered"].push(key.dailyrecovered)
    }):[]
    var _data = (this.props.data.india!==null && this.props.data.india!==undefined)?this.props.data.india.statewise.map((key:any,value:any)=>{
      if(key.state!=='Total'){
        data.push(
          {
            "state":key.state,
            "stateCode":key.statecode,
            "confirmed":parseInt(key.confirmed),
            "active":parseInt(key.active),
            "recovered":parseInt(key.recovered),
            "deaths":parseInt(key.deaths),
            "deltaconfirmed":Math.abs(parseInt(key.deltaconfirmed)),
            "deltarecovered":parseInt(key.deltarecovered),
            "deltadeaths":parseInt(key.deltadeaths),
            "masterDetail":"indiaData"
          }
        )
      }
      else{
        indiaData.push({
          "state":key.state,
          "stateCode":key.statecode,
          "confirmed":parseInt(key.confirmed),
          "active":parseInt(key.active),
          "recovered":parseInt(key.recovered),
          "deaths":parseInt(key.deaths),
          "deltaconfirmed":Math.abs(parseInt(key.deltaconfirmed)),
          "deltarecovered":parseInt(key.deltarecovered),
          "deltadeaths":parseInt(key.deltadeaths),
          "lastupdatedtime":key.lastupdatedtime
        })
      }
    }):[]
    return(
      <Grid
        container
        direction={"column"}
        spacing={3}
        >
        <Grid item xs={12}>
          <Route path="/india">
            <TopTab data={indiaData}/>
          </Route>
        </Grid>
        <Grid item xs={12}>
          <Route path ="/india" exact>
            <Grid>
              <LineGraph data={dataJson} filter={indiaLineFilter}/>
            </Grid>
            <Grid>
              <BarGraph data={dataJson} filter={indiaDailyLineFilter}/>
            </Grid>
          </Route>
          <Route path="/india/statewisereport" exact children={<Table data={data} template={indiaTableFilter}/>}/>
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = (state: any) => {
  return {
    data: state.data
  };
}

const mapDispatchToProps = {
  setIndiaData,
  setStateData,
  setDailyStateData
};

export default connect(mapStateToProps,mapDispatchToProps)(India);
