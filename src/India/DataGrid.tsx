import React from 'react';
import { connect } from 'react-redux';
import { Grid} from '@material-ui/core';
import { indiaDistrictTableFilter,getKeyByValue } from './../DeclarationFile';
import Table from "./../Component/Table";

class DataGridTable extends React.Component<any,any>{
  render(){
    // console.log(this.props)
    var data:any = (this.props.stateDetail.state!==undefined && this.props.stateDetail!==null && this.props.data.statedata!==null)?getKeyByValue(
      this.props.data.statedata,
      this.props.stateDetail.state
    ):[]
    var dataJSON:any = []
    var notInUse = (data!==[] && data!==undefined )?data.map((key:any,value:any)=>{
      Object.keys(key.districtData)
        .forEach(function eachKey(districtInfo:any) {
          // console.log(key.districtData[districtInfo])
          dataJSON.push({
            "district":districtInfo,
            "active":key.districtData[districtInfo].active,
            "confirmed":key.districtData[districtInfo].confirmed,
            "deceased":key.districtData[districtInfo].deceased,
            "recovered":key.districtData[districtInfo].recovered,
            "deltaconfirmed":key.districtData[districtInfo].deltaconfirmed,
            "deltarecovered":key.districtData[districtInfo].deltarecovered,
            "deltadeaths":key.districtData[districtInfo].deltadeaths
          })
        });
    }):[]
    return(
      <Grid>
        <Table data={dataJSON} template={indiaDistrictTableFilter}/>
      </Grid>
    )
  }
}

const mapStateToProps = (state: any) => {
  return {
    data: state.data
  };
}

export default connect(mapStateToProps)(DataGridTable);
